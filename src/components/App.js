import { useEffect, useState } from "react";
import "./App.css";
import { Table } from "./Table/Table";
import ReactTooltip from "react-tooltip";
import Refresh from "./Refresh";


function App() {
  const [data, setData] = useState("");
  const [pastData, setPastData] = useState(null);
  const [isHistory, setIsHistory] = useState(false);
  const [tableCheck, setTableCheck] = useState(true);
  const [currentDate, setCurrentDate] = useState(
    new Date().toLocaleDateString()
  );

  const pastState = [pastData, setPastData];
  const historyState = [isHistory, setIsHistory];
  const tableCheckState = [tableCheck, setTableCheck];
  const dateState = [currentDate, setCurrentDate];

  const notMonitoringText = "Please start monitoring!";
  
  useEffect(() => {
    window.chrome.storage.sync.get(null, function (data) {

      setData(data);
    });
  }, []);
  function getData() {
    window.chrome.storage.sync.get(null, function (data) {
      setPastData(null);
      setIsHistory(false);
      setTableCheck(true);
      setData(data);
      setCurrentDate(new Date().toLocaleDateString())
    });
  }
  return (
    <div className="App">
      <header id="attendance-title">
        <div className="logo-text">
          <img className="header-icon" src="/images/icon64.png" />
          <h1>Attendance Dashboard</h1>
        </div>
    <div  data-tip data-for="reloadTip"className="refresh-wrapper"onClick={getData} >
       <Refresh/>
    </div>
       

        <ReactTooltip  id="reloadTip" place="top" effect="solid">
          Reload
        </ReactTooltip>
      </header>
      {data.monitordata ? (
        <Table
          data={data}
          pastState={pastState}
          historyState={historyState}
          tableCheckState={tableCheckState}
          dateState={dateState}
        />
      ) : (
        <h2 className="monitor-absent">{notMonitoringText}</h2>
      )}
    </div>
  );
}

export default App;
