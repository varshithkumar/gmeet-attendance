export const COLUMNS =[
    {
        Header:"Attendee",
        accessor :"Attendee",
    },
    {
        Header:"Status",
        accessor :"Status",
        Filter:""
    },
    {
        Header:"Present",
        accessor :"Present",
        Filter:""

    },
    {
        Header:"Percentage",
        accessor :"Percentage",
        Filter:""

    },
]