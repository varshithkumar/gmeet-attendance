import React, { useEffect, useMemo, useState } from "react";
import { COLUMNS } from "./column";
import "./table.css";
import TableBody from "./TableBody";
import TableHead from "./TableHead"

export const Table = (props) => {
  const [currentDate, setCurrentDate] = [...props.dateState]

  const [pastData, setPastData] = [...props.pastState];
  const [isHistory,setIsHistory]= [...props.historyState];
  const [tableCheck, setTableCheck]= [...props.tableCheckState];

  const columns = useMemo(() => COLUMNS, []);
  const data = useMemo(() => {

    return createTable(props.data);
  }, [props.data]);


  function formatDate(input) {
    if (input.includes("-")) {
      return input.split("-").reverse().join("/");
    } else {
      return input.split("/").reverse().join("-");
    }
  }



  function createTable(data) {
    if (data && data.monitordata) {
      let obj = data.monitordata.attendance;
      if (obj.length > 0) {
        let analysisObjArr = [];
        let allNames = [];
        let allTimes = [];
        for (let i = 0; i < obj.length; i++) {
          for (let j = 0; j < obj[i].nameArr.length; j++) {
            if (!allNames.includes(obj[i].nameArr[j])) {
              allNames.push(obj[i].nameArr[j]);
            }
            if (!allTimes.includes(obj[i].time[j])) {
              allTimes.push(obj[i].time);
            }
          }
        }

        for (let m = 0; m < allNames.length; m++) {
          let tempobj = {
            Attendee: allNames[m],
            Status: false,
          };
          let count = 0;
          for (let n = 0; n < obj.length; n++) {
            if (obj[n].nameArr.includes(allNames[m])) {
              tempobj[window.chrome.i18n.getMessage("appPresent")] = tempobj[
                window.chrome.i18n.getMessage("appPresent")
              ]
                ? tempobj[window.chrome.i18n.getMessage("appPresent")] + 1
                : 1;
              tempobj.Status = obj[n].time;
              count++;
            }
            if (n == obj.length - 1) {
              tempobj.Percentage = ((count / obj.length) * 100).toFixed(2);
              tempobj.Status =
                tempobj.Status == allTimes[allTimes.length - 1]
                  ? window.chrome.i18n.getMessage("appStatusTrue")
                  : window.chrome.i18n.getMessage("appStatusFalse");
              analysisObjArr.push(tempobj);
            }
          }
        }
        return analysisObjArr;
      }
    }
  }

  //calendar
  function onCalendarChange(event) {

    let newDate = formatDate(event.target.value);
    setCurrentDate(newDate);
    setIsHistory(true)

    window.chrome.storage.local.get(null, (storageData) => {

      if (storageData.pastdata===undefined ||storageData.pastdata[newDate] === undefined) {

        setTableCheck(false);
        setPastData(null)
      } else {
      
        setTableCheck(true);
        setPastData(storageData.pastdata[newDate]);
      }
    });
  }

  return (
    <div className="container">
      <TableHead analysisArr={data} data={props.data} isHistory={isHistory} formatDate={formatDate}currentDate={currentDate} onCalendarChange={onCalendarChange}/>
      {tableCheck ? (
        <TableBody data={pastData ? pastData : data} columns={columns} />
      ) : (
        <h2>No Data present</h2>
      )}
    </div>
  );
};
