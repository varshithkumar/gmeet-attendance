import React, { useRef } from "react";
import CsvDownload from "react-json-to-csv";
import ReactTooltip from "react-tooltip";

export default function TableHead(props) {
  const {
    data,
    isHistory,
    formatDate,
    currentDate,
    onCalendarChange,
    analysisArr,
  } = { ...props };
  const inputEl = useRef(null);

  function exportJSONToCSV(objArray, name) {
    var arr = typeof objArray !== "object" ? JSON.parse(objArray) : objArray;
    var str =
      `${Object.keys(arr[0])
        .map((value) => `"${value}"`)
        .join(",")}` + "\r\n";
    var csvContent = arr.reduce((st, next) => {
      st +=
        `${Object.values(next)
          .map((value) => `"${value}"`)
          .join(",")}` + "\r\n";
      return st;
    }, str);

    var element = document.createElement("a");
    element.href = "data:text/csv;charset=utf-8," + encodeURI(csvContent);
    element.target = "_blank";
    element.download = `${name}.csv`;
    element.click();
  }

  const saveToStorage = () => {

    const dateVar = props.data.monitordata.date;
    let tempAnalysisArr = analysisArr;

    window.chrome.storage.local.get(null, (storageData) => {
      if (storageData.pastdata === undefined) {

        window.chrome.storage.local.set({
          pastdata: { [dateVar]: tempAnalysisArr },
        });
      } else {
        let tempObj = storageData.pastdata;

        tempObj[dateVar] = tempAnalysisArr;

        window.chrome.storage.local.set({ pastdata: tempObj });
      }
    });

  };

  const getMinDate = () => {
    var ourDate = new Date();

    var pastDate = ourDate.getDate() - 30;
    ourDate.setDate(pastDate);

    return formatDate(ourDate.toLocaleDateString());
  };

  return (
    <div class="table-title">
      {isHistory ? (
        <h2 className="type-title">History</h2>
      ) : (
        <h2 className="type-title">
          {data.type.charAt(0).toUpperCase() + data.type.slice(1) + " "}
          Monitoring ⏱️
        </h2>
      )}
      <div id="date-search">
        <input
          onChange={onCalendarChange}
          type="date"
          min={getMinDate()}
          max={formatDate(new Date().toLocaleDateString())}
          id="date"
          name="attendance-date"
          value={formatDate(currentDate)}
        ></input>
      </div>
      {isHistory ? null : (
        <button data-tip data-for="saveTip" className="save-btn" onClick={saveToStorage}>
          Save 💾
        </button>
      )}
      <div>

        <CsvDownload data-tip data-for="downloadTip" data={analysisArr}>Download</CsvDownload>
        <ReactTooltip id="downloadTip" place="top" effect="solid">
       Download
      </ReactTooltip>
        <ReactTooltip id="saveTip" place="top" effect="solid">
      Save to storage<br/> <span style={{color:"grey",fontSize: "0.7rem"}}>Please note: We are storing the data <br/>in local storage. Removing the extension <br/> clears all data ⚠️</span>
      </ReactTooltip>

      </div>
    </div>
  );
}
