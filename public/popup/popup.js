let startMonitorManual = document.querySelector(".startmonitor.manual");
let startMonitorAutomatic = document.querySelector(".startmonitor.automatic");
let takeNewAttendance = document.querySelector(".takenewattendance");
let cancel = document.querySelector(".cancel");
let downloadReport = document.querySelector(".downloadreport");
let count = document.querySelector(".count");
let quickAttendance = document.querySelector(".quickattandance");
let checkbox = document.querySelector(".checkbox");
let meetlink = document.querySelector(".meetlink");
let monitorMode = document.querySelector(".monitorMode");
let gridBtn = document.querySelector("#grid-btn");
let dashboardBtn = document.querySelector(".options > button");

function language() {
  document.querySelector(".popup-box h2 span").innerText =
    window.chrome.i18n.getMessage("appTitle");
  document.querySelector(".meetlink").innerText =
    window.chrome.i18n.getMessage("appInstantmeet");
  document.querySelector(".quickattandance span").innerText =
    window.chrome.i18n.getMessage("appQuickAttendance");
  document.querySelector(".count label").innerText =
    window.chrome.i18n.getMessage("appTotalAttendance");
  document.querySelector(".startmonitor.manual span").innerText =
    window.chrome.i18n.getMessage("appManualMonitor");
  document.querySelector(".startmonitor.automatic span").innerText =
    window.chrome.i18n.getMessage("appAutomaticMonitor");
  document.querySelector(".takenewattendance span").innerText =
    window.chrome.i18n.getMessage("appTakeNewAttendance");
  document.querySelector(".cancel span").innerText =
    window.chrome.i18n.getMessage("appCancel");
  document.querySelector(".downloadreport span").innerText =
    window.chrome.i18n.getMessage("appDonwloadReport");
  document.querySelector(".toggle h3").innerText =
    window.chrome.i18n.getMessage("appAdmitTitle");
  document.querySelector(".toggle small").innerText =
    window.chrome.i18n.getMessage("appAdmitDesc");
  document.querySelector(".error").innerText =
    window.chrome.i18n.getMessage("appRedirect");
}

language();
// let snapshot = document.querySelector(".snapshot");

chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
  if (
    tabs[0].url.includes("https://meet.google.com/") &&
    tabs[0].url.length > 30
  ) {
    document.querySelector(".popup-body").style.display = "flex";
    document.querySelector(".error").style.display = "none";
    document.querySelector(".togglediv").style.display = "flex";
  } else {
    document.querySelector(".popup-body").style.display = "none";
    document.querySelector(".error").style.display = "flex";
    document.querySelector(".togglediv").style.display = "none";
  }
});

chrome.storage.sync.get(null, (data) => {
  console.log(data)
  let type = ""
  if (data.type) {
      type = data.type;
  }
  if (data.autoAccept == true) {
      document.querySelector(".checkbox input").checked = true
  } else {
      document.querySelector(".checkbox input").checked = false
  }
  console.log(Object.keys(data).length)
  if ((Object.keys(data).length) == 2 || (Object.keys(data).length) == 3) {
      startMonitorManual.style.display = "flex";
      startMonitorAutomatic.style.display = "flex";
      count.style.display = "none";
      takeNewAttendance.style.display = "none";
      cancel.style.display = "none";
      downloadReport.style.display = "none";
      monitorMode.style.display = "none";
  } else {
      startMonitorManual.style.display = "none";
      startMonitorAutomatic.style.display = "none";
      if (type == "automatic") {
          takeNewAttendance.style.display = "none";
          monitorMode.innerText = window.chrome.i18n.getMessage("appAutomaticMode");
          monitorMode.style.display = "unset";
      } else if (type == "manual") {
          takeNewAttendance.style.display = "flex";
          monitorMode.innerText = window.chrome.i18n.getMessage("appManualMode");
          monitorMode.style.display = "unset";
      }
      count.style.display = "flex";
      cancel.style.display = "flex";
      downloadReport.style.display = "flex";
      document.querySelector(".count span").innerText = data.monitordata.attendance.length;
  }
})

 


startMonitorManual.addEventListener("click", () => {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { action: "startmonitor", type: "manual" },
      function (response) {
        console.log(response);
        if (response.message == "done") {
          startMonitorManual.style.display = "none";
          startMonitorAutomatic.style.display = "none";
          takeNewAttendance.style.display = "flex";
          cancel.style.display = "flex";
          downloadReport.style.display = "flex";
          monitorMode.innerText =
            window.chrome.i18n.getMessage("appManualMode");
          monitorMode.style.display = "unset";
          setTimeout(() => {
            chrome.storage.sync.get("monitordata", (data) => {
              count.style.display = "flex";
              document.querySelector(".count span").innerText =
                data.monitordata.attendance.length;
              startMonitorManual.querySelector("span").innerText =
                "start Monitor";
            });
          }, 500);
        }
      }
    );
  });
});

startMonitorAutomatic.addEventListener("click", () => {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { action: "startmonitor", type: "automatic" },
      function (response) {
        console.log(response);
        if (response.message == "done") {
          startMonitorManual.style.display = "none";
          startMonitorAutomatic.style.display = "none";
          takeNewAttendance.style.display = "none";
          cancel.style.display = "flex";
          downloadReport.style.display = "flex";
          monitorMode.innerText =
            window.chrome.i18n.getMessage("appAutomaticMode");
          monitorMode.style.display = "unset";
          setTimeout(() => {
            chrome.storage.sync.get("monitordata", (data) => {
              count.style.display = "flex";
              document.querySelector(".count span").innerText =
                data.monitordata.attendance.length;
              startMonitorAutomatic.querySelector("span").innerText =
                "start Monitor";
            });
          }, 500);
        }
      }
    );
  });
});

takeNewAttendance.addEventListener("click", () => {
  takeNewAttendance.querySelector("span").innerText = "Analysing";
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { action: "takenewattendance" },
      function (response) {
        console.log(response);
        if (response.message == "done") {
          setTimeout(() => {
            chrome.storage.sync.get("monitordata", (data) => {
              document.querySelector(".count span").innerText =
                data.monitordata.attendance.length;
            });
            takeNewAttendance.querySelector("span").innerText =
              "Take new Attendance";
          }, 500);
        }
      }
    );
  });
});

cancel.addEventListener("click", () => {
  startMonitorManual.style.display = "flex";
  startMonitorAutomatic.style.display = "flex";
  count.style.display = "none";
  takeNewAttendance.style.display = "none";
  cancel.style.display = "none";
  downloadReport.style.display = "none";
  monitorMode.style.display = "none";
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { action: "cancel" },
      function (response) {
        language();
      }
    );
  });
});

downloadReport.addEventListener("click", () => {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { action: "downloadreport" },
      function (response) {}
    );
  });
});

quickAttendance.addEventListener("click", () => {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id,
      { action: "quickattandance" },
      function (response) {}
    );
  });
});

checkbox.addEventListener("click", () => {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.storage.sync.get(null, (data) => {
      if (data.autoAccept == true) {
        chrome.storage.sync.set({ autoAccept: false });
      } else {
        chrome.storage.sync.set({ autoAccept: true });
      }
    });
  });
});

meetlink.addEventListener("click", () => {
  chrome.runtime.sendMessage({ action: "meetlink" }, function (response) {});
});

// snapshot.addEventListener("click", () => {
//     chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
//         chrome.tabs.sendMessage(tabs[0].id, { action: "snapshot" }, function (response) { });
//     });
// })

gridBtn.addEventListener("click", () => {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      chrome.tabs.sendMessage(
        tabs[0].id,
        { action: "display-grid-view" },
        function (response) {
          console.log(response);
        }
      );
    });
    window.close();
  });
  
  dashboardBtn.addEventListener("click", () => {
    chrome.tabs.create({
      url: chrome.runtime.getURL("index.html"),
    });
    chrome.runtime.sendMessage({
      msg: "from popup", action: "clear_old_data"
     
  });
  });
  