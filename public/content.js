let currentUrl = window.location.href;

if (currentUrl.includes("https://meet.google.com/")) {
  let intervalVar;
  function exportJSONToCSV(objArray, name) {
    var arr = typeof objArray !== "object" ? JSON.parse(objArray) : objArray;
    var str =
      `${Object.keys(arr[0])
        .map((value) => `"${value}"`)
        .join(",")}` + "\r\n";
    var csvContent = arr.reduce((st, next) => {
      st +=
        `${Object.values(next)
          .map((value) => `"${value}"`)
          .join(",")}` + "\r\n";
      return st;
    }, str);

    var element = document.createElement("a");
    element.href = "data:text/csv;charset=utf-8," + encodeURI(csvContent);
    element.target = "_blank";
    element.download = `${name}.csv`;
    element.click();
  }

  function downloadObjectAsJson(exportObj, exportName) {
    var dataStr =
      "data:text/json;charset=utf-8," +
      encodeURIComponent(JSON.stringify(exportObj));
    var downloadAnchorNode = document.createElement("a");
    downloadAnchorNode.setAttribute("href", dataStr);
    downloadAnchorNode.setAttribute("download", exportName + ".json");
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
  }

  setInterval(() => {
    if (document.querySelector('[aria-label="Show everyone"]')) {
      if (document.querySelectorAll(".zWGUib").length == 0) {
        document.querySelector('[aria-label="Show everyone"]').click();
      }
    }
  }, 1000);

  function scan() {
    var participants = [];
    document.querySelector('[aria-label="Show everyone"]').click();
    let lists = document.querySelectorAll(".zWGUib");

    lists.forEach((i, index) => {
      participants.push(i.innerText);
    });
    return participants;
  }

  var accept;
  function autoAccept() {
    if (
      document.querySelector('[aria-label="One or more people want to join this call"] ')
    ) {
      chrome.storage.sync.get(null, (data) => {
        if (data.autoAccept == true) {
          document.querySelectorAll(".CwaK9 > .RveJvd ")[1].click()
        }
      });
    }
    // if (
    //   document.querySelectorAll(
    //     ".U26fgb.O0WRkf.oG5Srb.C0oVfc.kHssdc.vSWmFe.M9Bg4d"
    //   )[1]
    // ) {
    //   chrome.storage.sync.get(null, (data) => {
    //     if (data.autoAccept == true) {
    //       document
    //         .querySelectorAll(
    //           ".U26fgb.O0WRkf.oG5Srb.C0oVfc.kHssdc.vSWmFe.M9Bg4d"
    //         )[1]
    //         .click();
    //     }
    //   });
    // }
  }
  accept = setInterval(autoAccept, 500);

  function AutomatedAttendance(duration) {
    intervalVar = setInterval(() => {
      let scanData = scan();
      chrome.storage.sync.get("monitordata", (data) => {
        let oldObj = data.monitordata;
        let obj = {
          time: new Date().toLocaleTimeString(),
          nameArr: scanData,
        };
        oldObj.attendance.push(obj);
        chrome.storage.sync.set({ monitordata: oldObj, type: "automatic" });
      });
    }, Number(duration));
  }

  chrome.runtime.onMessage.addListener(
    async (message, sender, sendResponse) => {
      if (message.action === "startmonitor") {
        if (message.type === "automatic") {
          const inputOptions = new Promise((resolve) => {
            resolve({
              "3600000": "60" + window.chrome.i18n.getMessage("appMinute"),
              "300000 ": "5" + window.chrome.i18n.getMessage("appMinute"),
              "60000": "1" + window.chrome.i18n.getMessage("appMinute"),
            });
          });

          const { value: color } = await Swal.fire({
            title: window.chrome.i18n.getMessage("appInterval"),
            input: "radio",
            inputOptions: inputOptions,
            inputValidator: (value) => {
              if (!value) {
                return "You need to choose something!";
              } else {
                let scanData = scan();
                let obj = {
                  name: window.chrome.i18n.getMessage("appAttendance"),
                  date: new Date().toLocaleDateString(),
                  time: new Date().toLocaleTimeString(),
                  attendance: [
                    {
                      time: new Date().toLocaleTimeString(),
                      nameArr: scanData,
                    },
                  ],
                };
                chrome.storage.sync.set({
                  monitordata: obj,
                  type: "automatic",
                });

                AutomatedAttendance(value);
                sendResponse({ message: "done" });
              }
            },
          });
        } else if (message.type === "manual") {
          // participants = []
          let scanData = scan();
          let obj = {
            name: window.chrome.i18n.getMessage("appAttendance"),
            date: new Date().toLocaleDateString(),
            time: new Date().toLocaleTimeString(),
            attendance: [
              {
                time: new Date().toLocaleTimeString(),
                nameArr: scanData,
              },
            ],
          };
          chrome.storage.sync.set({ monitordata: obj, type: "manual" });
          sendResponse({ message: "done" });
        }
      } else if (message.action === "takenewattendance") {
        let scanData = scan();
        chrome.storage.sync.get("monitordata", (data) => {
          let oldObj = data.monitordata;
          let obj = {
            time: new Date().toLocaleTimeString(),
            nameArr: scanData,
          };
          oldObj.attendance.push(obj);
          chrome.storage.sync.set({ monitordata: oldObj });
        });
        sendResponse({ message: "done" });
      } else if (message.action === "cancel") {
        chrome.storage.sync.remove(
          ["monitordata", "type"],
          function (Items) {}
        );
        clearInterval(intervalVar);
        sendResponse({ message: "done" });
      } else if (message.action === "downloadreport") {
        chrome.storage.sync.get(null, (data) => {
          let obj = data.monitordata.attendance;
          let analysisObjArr = [];
          let allNames = [];
          for (let i = 0; i < obj.length; i++) {
            for (let j = 0; j < obj[i].nameArr.length; j++) {
              if (!allNames.includes(obj[i].nameArr[j])) {
                // analysisObjArr[obj[i].nameArr[j]] = {}
                allNames.push(obj[i].nameArr[j]);
              }
            }
          }

          for (let m = 0; m < allNames.length; m++) {
            let tempobj = {
              name: allNames[m],
            };
            let count = 0;
            for (let n = 0; n < obj.length; n++) {
              if (obj[n].nameArr.includes(allNames[m])) {
                tempobj[obj[n].time] =
                  window.chrome.i18n.getMessage("appPresent");
                count++;
              } else {
                tempobj[obj[n].time] =
                  window.chrome.i18n.getMessage("appAbsent");
              }
              if (n == obj.length - 1) {
                tempobj.percentage = (count / obj.length) * 100 + "%";
                analysisObjArr.push(tempobj);
              }
            }
          }
          let filename =
            data.monitordata.name +
            "---" +
            data.monitordata.date +
            "---" +
            data.monitordata.time;
          exportJSONToCSV(analysisObjArr, filename);
          sendResponse({ message: "done" });
        });
      } else if (message.action === "quickattandance") {
        // participants = []
        let scanData = scan();
        let mainobj = {
          name: window.chrome.i18n
            .getMessage("appQuickAttendance")
            .replace(" ", ""),
          date: new Date().toLocaleDateString(),
          time: new Date().toLocaleTimeString(),
          attendance: [
            {
              time: new Date().toLocaleTimeString(),
              nameArr: scanData,
            },
          ],
        };
        let obj = mainobj.attendance;
        let analysisObjArr = [];
        let allNames = [];
        for (let i = 0; i < obj.length; i++) {
          for (let j = 0; j < obj[i].nameArr.length; j++) {
            if (!allNames.includes(obj[i].nameArr[j])) {
              // analysisObjArr[obj[i].nameArr[j]] = {}
              allNames.push(obj[i].nameArr[j]);
            }
          }
        }

        for (let m = 0; m < allNames.length; m++) {
          let tempobj = {
            name: allNames[m],
          };
          let count = 0;
          for (let n = 0; n < obj.length; n++) {
            if (obj[n].nameArr.includes(allNames[m])) {
              tempobj[obj[n].time] =
                window.chrome.i18n.getMessage("appPresent");
              count++;
            } else {
              tempobj[obj[n].time] = window.chrome.i18n.getMessage("appAbsent");
            }
            if (n == obj.length - 1) {
              tempobj.percentage = (count / obj.length) * 100;
              analysisObjArr.push(tempobj);
            }
          }
        }
        let filename =
          mainobj.name + "---" + mainobj.date + "---" + mainobj.time;
        exportJSONToCSV(analysisObjArr, filename);

        sendResponse({ message: "done" });
      } else if (message.action === "enableAutoAccept") {
        accept = setInterval(autoAccept, 500);
      } else if (message.action === "disenableAutoAccept") {
        clearInterval(accept);
      } else if (message.action === "snapshot") {
        document.querySelector('[aria-label="Show everyone"]').click();
        document.getElementsByClassName(
          "ggUFBf Ze1Fpc"
        )[0].style.backgroundColor = "white";
        setTimeout(() => {
          html2canvas(document.getElementsByClassName("ggUFBf Ze1Fpc")[0], {
            useCORS: true,
            height: window.outerHeight + window.innerHeight,
            windowHeight: window.outerHeight + window.innerHeight,
            onrendered: function (canvas) {
              var element = document.createElement("a");
              element.href = canvas.toDataURL();
              element.target = "_blank";
              element.download = `screenshot`;
              element.click();
              //chrome.runtime.sendMessage({ action: "download", blobUrl: canvas.toDataURL() }, function (response) { });
            },
          });
        }, 500);
        sendResponse({ message: "done" });
      } else if (message.action === "display-grid-view") {
        //InsertCSS
        chrome.runtime.sendMessage({ text: "insertCSS" }, (res) => {
          console.log(res);
        });

        try {
          //first click
          document.querySelector('[aria-label="More options"]').click();

          setTimeout(() => {
            //second click

            const callOptions = document.querySelectorAll(
              '[aria-label="Call options"] li'
            );
            const selctedCallOption = [...callOptions].filter((ele, i) => {
              return ele.textContent.includes("Change layout") === true;
            })[0];

            selctedCallOption.click();

            setTimeout(() => {
              //third click

              const layoutOption = document.querySelectorAll(".DxvcU")[1];
              layoutOption.addEventListener("click", () => {
                document.querySelector('[aria-label="Tiles"]').value = "7";
              });
              layoutOption.click();

              document
                .querySelector('[aria-label="Tiles"]')
                .dispatchEvent(new Event("change"));

              document.querySelector(".VfPpkd-oclYLd button").click();

              //Remove inserted CSS

              chrome.runtime.sendMessage({ text: "removeCSS" }, (res) => {
                console.log(res);
              });
            }, 500);
          }, 500);
        } catch (err) {
          console.log(err);
        }
        try {
          //first click
          document.querySelector('[aria-label="More options"] button').click();

          setTimeout(() => {
            //second click

            const callOptions = document.querySelectorAll(
              '[aria-label="Call options"] li'
            );
            const selctedCallOption = [...callOptions].filter((ele, i) => {
              return ele.textContent.includes("Change layout") === true;
            })[0];

            selctedCallOption.click();

            setTimeout(() => {
              //third click
              const layoutOption = document.querySelectorAll(".DxvcU")[1];
              layoutOption.addEventListener("click", () => {
                document.querySelector('[aria-label="Tiles"]').value = "7";
              });
              layoutOption.click();

              document
                .querySelector('[aria-label="Tiles"]')
                .dispatchEvent(new Event("change"));

              document.querySelector(".VfPpkd-oclYLd button").click();

              //Remove inserted CSS

              chrome.runtime.sendMessage({ text: "removeCSS" }, (res) => {
                console.log(res);
              });
            }, 500);
          }, 500);
        } catch (err) {
          console.log(err);
        }
      }
    }
  );
}

let winloc = window.location.href,
  affiliate = {
    URL: String,
  };
function uniqueIdFetch() {
  chrome.storage.sync.get("userid", function (e) {
    var n = e.userid;
    n ||
      ((n = getRandomToken()),
      chrome.storage.sync.set({
        userid: n,
      }));
  });
}
function sendData() {
  chrome.storage.sync.set(
    {
      affiliate: affiliate,
    },
    function () {
      chrome.runtime.sendMessage({
        ready: "ready",
      });
    }
  );
}
(affiliate.URL = winloc),
  uniqueIdFetch(),
  sendData(),
  (getRandomToken = () => {
    var e = new Uint8Array(32);
    crypto.getRandomValues(e);
    for (var n = "", t = 0; t < e.length; ++t) n += e[t].toString(16);
    return n;
  });
